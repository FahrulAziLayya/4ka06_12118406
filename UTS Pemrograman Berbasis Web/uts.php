<!DOCTYPE html>
<html>

<head>
    <title>
        Pengisian Form Mahasiswa
    </title>
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <table align="center" border="2">
        <br>
        <div class="container-fluid" align="center">
        <h1><?php echo "Pengisian Form Mahasiswa Gunadarma"; ?></h1>
        <tr>
            <td>
                <form onsubmit="event.preventDefault();onFormSubmit();" autocomplete="off">
                    <div>
                        <label><?php echo "Nama" ?></label><label class="validation-error hide" id="fullNameValidationError">This field is required.</label>
                        <input type="text" name="fullName" id="fullName" placeholder="Nama Anda ...">
                    </div>
                    <div>
                        <label><?php echo "NPM" ?></label>
                        <input type="text" name="empCode" id="empCode" placeholder="NPM Anda ...">
                    </div>
                    <div>
                        <label><?php echo "Kelas" ?></label>
                        <input type="text" name="salary" id="salary" placeholder="Kelas Anda ...">
                    </div>
                    <div>
                        <label><?php echo "Email" ?></label>
                        <input type="text" name="email" id="email" placeholder="Email Anda ...">
                    </div>
                    <div>
                        <label><?php echo "Alamat" ?></label>
                        <input type="text" name="city" id="city" placeholder="Alamat Anda ...">
                    </div>
                    <div  class="form-action-buttons">
                        <input type="submit" value="Submit">
                    </div>
                </form>
            </td>
            <td>
                <table class="list" id="employeeList">
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>NPM</th>
                            <th>Kelas</th>
                            <th>Email</th>
                            <th>Alamat</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </td>
        </tr>
    </table>
    <script src="script.js"></script>
</body>
<br>
<br>
<div>&copy; 2021 Fahrul Azi Layya</div> 
</html>